import { ErrorBoundary } from 'src/shared/error-boundary';
import { FullPageError } from 'src/shared/full-page-error';
import { MainContainer } from 'src/containers/main';
import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

export function AppRouter() {
  return (
    <ErrorBoundary fallback={<FullPageError />}>
      <Router>
        <MainContainer />
      </Router>
    </ErrorBoundary>
  );
}
