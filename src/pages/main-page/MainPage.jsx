import { Card, Container, Grid, makeStyles } from '@material-ui/core';
import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { TournamentItem } from 'src/shared/tournament-item';
import { removeTournament } from 'src/core/actions';

export function MainPage(props) {
  const classes = useStyles(props);

  const { tournaments } = useSelector((state) => state.tournaments);

  const dispatch = useDispatch();

  const handleDelete = useCallback((id) => dispatch(removeTournament(id)), [
    dispatch,
  ]);

  return (
    <Container maxWidth="xl" className={classes.root}>
      <Grid container spacing={2} alignContent="center" justify="center">
        {tournaments.map((tournament) => (
          <Grid
            item
            key={'MainPageTournament:' + tournament.id}
            className={classes.tournamentGridItem}
          >
            <Card>
              <TournamentItem
                title={tournament.title}
                subtitle={tournament.description}
                image={
                  tournament.images.square?.filePath ??
                  tournament.images.default?.filePath
                }
                id={tournament.id}
                onDelete={handleDelete}
              />
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}

const styles = (theme) => ({
  root: {
    padding: theme.spacing(3),
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing(2, 0.5),
    },
  },
  tournamentGridItem: {
    [theme.breakpoints.down('xs')]: {
      maxWidth: '100%',
    },
  },
});

const useStyles = makeStyles(styles, 'MainPage');
