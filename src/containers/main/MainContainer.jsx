import { ContentRegion, MainLayout, NavbarRegion } from 'src/shared/layouts';

import { ErrorBoundary } from 'src/shared/error-boundary';
import { FullPageError } from 'src/shared/full-page-error';
import { MainRouter } from './MainRouter';
import React from 'react';
import { SearchBar } from 'src/shared/search-bar';

export function MainContainer() {
  return (
    <MainLayout>
      <NavbarRegion>
        <SearchBar />
      </NavbarRegion>
      <ContentRegion>
        <ErrorBoundary fallback={<FullPageError />}>
          <MainRouter />
        </ErrorBoundary>
      </ContentRegion>
    </MainLayout>
  );
}
