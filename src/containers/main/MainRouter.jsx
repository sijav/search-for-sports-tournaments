import { Route, Switch } from 'react-router-dom';

import { MainPage } from 'src/pages/main-page';
import React from 'react';

export function MainRouter() {
  return (
    <Switch>
      <Route path="/">
        <MainPage />
      </Route>
    </Switch>
  );
}
