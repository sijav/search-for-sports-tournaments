import { darkTheme, lightTheme } from './main-themes';

import React from 'react';
import { ThemeProvider } from '@material-ui/core';
import { useSelector } from 'react-redux';

export function MainThemeProvider(props) {
  const { children } = props;
  const { darkMode } = useSelector((state) => state.theme);
  return (
    <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
      {children}
    </ThemeProvider>
  );
}
