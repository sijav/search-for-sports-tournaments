import { blue, grey, lightBlue, red } from '@material-ui/core/colors';

import { createMuiTheme } from '@material-ui/core';

export function getThemeOptions(base, paletteType) {
  const fontFamily = '"Roboto", "Helvetica", "Arial", sans-serif';

  const typography = {
    fontFamily,
    h1: { fontSize: 36, fontWeight: 'bold' },
    h2: { fontSize: 30, fontWeight: 'bold' },
    h3: { fontSize: 24, fontWeight: 'bold' },
    h4: { fontSize: 20, fontWeight: 'bold' },
    h5: { fontSize: 18, fontWeight: 'bold' },
    h6: { fontSize: 16, fontWeight: 'bold' },
    subtitle1: { fontSize: 12 },
    subtitle2: { fontSize: 11, fontWeight: 'bold' },
    body1: { fontSize: 14 },
    body2: { fontSize: 12 },
    button: { fontSize: 14, fontWeight: 'bold' },
    caption: { fontSize: 12, fontWeight: 'lighter' },
    overline: { fontSize: 13, fontWeight: 'lighter' },
  };

  return {
    ...base,
    palette: {
      ...base.palette,
      type: paletteType,
      background: { default: grey[100] },
    },
    typography,
    overrides: {
      MuiTypography: {
        colorTextPrimary: {
          color: '#212121',
        },
        colorTextSecondary: {
          color: '#616161',
        },
      },
      MuiInputBase: {
        root: {
          fontSize: typography?.body1?.fontSize,
        },
      },
    },
  };
}

export const baseThemeOptions = {
  palette: {
    primary: blue,
    secondary: lightBlue,
    error: red,
  },
};

export const lightTheme = createMuiTheme(
  getThemeOptions(baseThemeOptions, 'light')
);

export const darkTheme = createMuiTheme(
  getThemeOptions(baseThemeOptions, 'dark')
);
