import { SEARCH_SET } from 'src/core/actions/types';

const INIT_STATE = {
  data: [],
};

export function searchReducer(state = INIT_STATE, action) {
  switch (action.type) {
    case SEARCH_SET:
      return {
        ...state,
        data: [...action.payload],
      };
    default:
      return state;
  }
}
