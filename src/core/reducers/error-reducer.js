import { ERROR_RESET, ERROR_SET } from 'src/core/actions/types';

const INIT_STATE = {
  error: null,
  reset: () => {},
};

export function errorReducer(state = INIT_STATE, action) {
  switch (action.type) {
    case ERROR_SET:
      return {
        ...state,
        error: action.payload.error,
        reset: action.payload.reset,
      };
    case ERROR_RESET:
      return { ...state, error: null, reset: () => {} };
    default:
      return state;
  }
}
