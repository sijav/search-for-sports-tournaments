import { THEME_DARK_MODE_SET } from 'src/core/actions/types';

const INIT_STATE = {
  darkMode: false,
};

export function themeReducer(state = INIT_STATE, action) {
  switch (action.type) {
    case THEME_DARK_MODE_SET:
      return { ...state, darkMode: action.payload };
    default:
      return state;
  }
}
