import { combineReducers } from 'redux';
import { errorReducer } from './error-reducer';
import { persistReducer } from 'redux-persist';
import { searchReducer } from './search-reducer';
import storage from 'redux-persist/es/storage';
import { themeReducer } from './theme-reducer';
import { tournamentsReducer } from './tournaments-reducer';

function createPersistConfig(reducerName) {
  return {
    key: reducerName,
    storage,
  };
}

export const reducers = combineReducers({
  theme: persistReducer(createPersistConfig('theme'), themeReducer),
  error: errorReducer,
  tournaments: persistReducer(
    createPersistConfig('tournaments'),
    tournamentsReducer
  ),
  search: searchReducer,
});
