import { TOURNAMENTS_ADD, TOURNAMENTS_REMOVE } from 'src/core/actions/types';

const INIT_STATE = {
  tournaments: [],
};

export function tournamentsReducer(state = INIT_STATE, action) {
  const tournaments = [...state.tournaments];
  switch (action.type) {
    case TOURNAMENTS_ADD:
      if (
        tournaments.findIndex(
          (tournament) => tournament.id === action.payload.id
        ) < 0
      ) {
        tournaments.push(action.payload);
      }
      return {
        ...state,
        tournaments,
      };
    case TOURNAMENTS_REMOVE:
      const selectedTournamentIndex = tournaments.findIndex(
        (tournament) => tournament.id === action.payload
      );
      if (selectedTournamentIndex > -1) {
        tournaments.splice(selectedTournamentIndex, 1);
      }
      return { ...state, tournaments };
    default:
      return state;
  }
}
