import { createStore } from 'redux';
import { persistStore } from 'redux-persist';
import { reducers } from 'src/core/reducers';

export function configureMainStore(initialState) {
  const store = createStore(reducers, initialState);
  const persistor = persistStore(store);
  return { store, persistor };
}
