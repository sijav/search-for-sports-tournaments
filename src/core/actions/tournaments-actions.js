export const TOURNAMENTS_ADD = 'TOURNAMENTS_ADD';
export const TOURNAMENTS_REMOVE = 'TOURNAMENTS_REMOVE';

export function addTournament(tournament) {
  return {
    type: TOURNAMENTS_ADD,
    payload: tournament,
  };
}

export function removeTournament(id) {
  return {
    type: TOURNAMENTS_REMOVE,
    payload: id,
  };
}
