export const SEARCH_SET = 'SEARCH_SET';

export function setSearch(data) {
  return {
    type: SEARCH_SET,
    payload: data,
  };
}
