export const THEME_DARK_MODE_SET = 'THEME_DARK_MODE_SET';

export function darkModeAction(isDarkMode) {
  return {
    type: THEME_DARK_MODE_SET,
    payload: isDarkMode,
  };
}
