export const ERROR_SET = 'ERROR_SET';
export const ERROR_RESET = 'ERROR_RESET';

export function setErrorAction(error, reset = () => {}) {
  return {
    type: ERROR_SET,
    payload: { error, reset },
  };
}

export function resetErrorAction() {
  return {
    type: ERROR_RESET,
    payload: { error: null, reset: () => {} },
  };
}
