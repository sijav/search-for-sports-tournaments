import { createStyles, makeStyles } from '@material-ui/core';

export function GlobalStyles(props) {
  useStyles(props);
  return null;
}

const styles = () =>
  createStyles({
    '@global': {
      html: {},
      body: {
        direction: 'ltr',
      },
    },
  });

const useStyles = makeStyles(styles, { name: 'GlobalStyles' });
