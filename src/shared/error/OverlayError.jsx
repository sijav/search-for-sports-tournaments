import { Button, Typography, makeStyles } from '@material-ui/core';

import { amber as Amber } from '@material-ui/core/colors';
import { Overlay } from 'src/shared/overlay';
import React from 'react';
import WarningRounded from '@material-ui/icons/WarningRounded';
import { useSelector } from 'react-redux';

export function OverlayError(props) {
  const { buttonText = 'Try Again' } = props;
  const classes = useStyles(props);
  const {
    reset,
    error = props.errorText ?? 'Something went wrong. Please try again',
  } = useSelector((state) => state.error);

  return (
    <Overlay>
      <WarningRounded style={{ fontSize: 100, color: Amber['400'] }} />
      <Typography variant="h3">{'Error'}</Typography>
      <Typography className={classes.label}>{error}</Typography>
      {!!reset && (
        <Button onClick={reset} variant="outlined" color="primary">
          {buttonText}
        </Button>
      )}
    </Overlay>
  );
}

const styles = (theme) => ({
  label: {
    textAlign: 'center',
    maxWidth: 240,
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
});

const useStyles = makeStyles(styles, { name: 'OverlayError' });
