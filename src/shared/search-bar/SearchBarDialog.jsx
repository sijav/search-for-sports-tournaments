import { Card, CardHeader, Popper, makeStyles } from '@material-ui/core';

import React from 'react';
import SearchItem from './SearchItem';
import { useSelector } from 'react-redux';

export function SearchBarDialog(props) {
  const { inputRef, inputHasFocus } = props;
  const classes = useStyles(props);
  const { data } = useSelector((state) => state.search);
  return (
    <Popper
      open={inputHasFocus && data.length > 0}
      anchorEl={inputRef.current}
      className={classes.root}
    >
      {data.map((category) => (
        <Card key={'SearchBarDialogCategory:' + category.category}>
          <CardHeader title={category.category.toUpperCase()} />
          {category.documents.map((document) => (
            <SearchItem
              document={document}
              key={'SearchBarDialogDocument:' + document.id}
            />
          ))}
        </Card>
      ))}
    </Popper>
  );
}

const styles = (theme) => ({
  root: {
    zIndex: 1100,
    maxHeight: 'calc(100vh - 75px)',
    maxWidth: 'calc(100vw - 12px)',
    overflow: 'auto',
  },
});

const useStyles = makeStyles(styles, 'SearchBarDialog');
