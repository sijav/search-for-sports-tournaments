import React, { useCallback } from 'react';

import { Divider } from '@material-ui/core';
import { TournamentItem } from '../tournament-item/TournamentItem';
import { addTournament } from 'src/core/actions';
import { useDispatch } from 'react-redux';

export default function SearchItem(props) {
  const { document } = props;
  const title = document.title;
  const subtitle = document.description;
  const image =
    document.images.square?.filePath ?? document.images.default?.filePath;

  const dispatch = useDispatch();
  const handleClick = useCallback(() => dispatch(addTournament(document)), [
    dispatch,
    document,
  ]);

  return (
    <>
      <TournamentItem
        title={title}
        subtitle={subtitle}
        image={image}
        onClick={handleClick}
      />
      <Divider orientation="horizontal" variant="fullWidth" />
    </>
  );
}
