import { InputBase, fade, makeStyles } from '@material-ui/core';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';

import { SearchBarDialog } from './SearchBarDialog';
import SearchIcon from '@material-ui/icons/Search';
import { getUniqueClient } from 'src/shared/utils/http';
import { setSearch } from 'src/core/actions';
import { useDebouncedCallback } from 'use-debounce';
import { useDispatch } from 'react-redux';

export function SearchBar(props) {
  const classes = useStyles(props);
  const dispatch = useDispatch();
  const inputRef = useRef();

  const [searchCriteria, setSearchCriteria] = useState('');
  const [inputHasFocus, setInputHasFocus] = useState(false);

  const debouncedSearchCriteria = useDebouncedCallback(setSearchCriteria, 350);
  const debouncedInputFocus = useDebouncedCallback(setInputHasFocus, 150);

  const handleChange = useCallback(
    (event) => debouncedSearchCriteria.callback(event.target.value),
    [debouncedSearchCriteria]
  );

  const handleFocus = useCallback(() => {
    debouncedInputFocus.callback(true);
  }, [debouncedInputFocus]);

  const handleBlur = useCallback(() => {
    debouncedInputFocus.callback(false);
  }, [debouncedInputFocus]);

  const client = useMemo(getUniqueClient, []);

  const get = useMemo(
    () =>
      client.get(
        (data) => dispatch(setSearch(data)),
        () => {}
      ),
    [client, dispatch]
  );

  useEffect(() => {
    if (searchCriteria) {
      get(`search?q=${searchCriteria}&index=tournament`);
    }
    return () => {
      if (client.cancel) {
        client.cancel();
      }
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchCriteria]);

  return (
    <div className={classes.search}>
      <div className={classes.searchIcon}>
        <SearchIcon />
      </div>
      <InputBase
        placeholder="Type here to search tournaments…"
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput,
        }}
        onChange={handleChange}
        onBlur={handleBlur}
        onFocus={handleFocus}
        inputProps={{ 'aria-label': 'search' }}
        inputRef={inputRef}
      />
      <SearchBarDialog inputHasFocus={inputHasFocus} inputRef={inputRef} />
    </div>
  );
}

const styles = (theme) => ({
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    margin: 'auto',
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 250,
      '&:focus': {
        width: 300,
      },
    },
  },
});

const useStyles = makeStyles(styles, { name: 'SearchBar' });
