import { OverlayError } from 'src/shared/error';
import React from 'react';

export function FullPageError() {
  return <OverlayError />;
}
