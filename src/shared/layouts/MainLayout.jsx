import { AppBar, Toolbar, makeStyles } from '@material-ui/core';

import React from 'react';
import { groupChildrenByType } from 'src/shared/utils/groupChildrenByType';
import { headerHeight } from './constants';

// typescript only allows string when it defined at `JSX.IntrinsicElements`
export const ContentRegion = 'ContentRegion';
export const NavbarRegion = 'NavbarRegion';

const regions = [NavbarRegion, ContentRegion];

export function MainLayout(props) {
  const classes = useStyles(props);

  const map = groupChildrenByType(props.children, regions);
  const contentChild = map.get(ContentRegion);
  const navbarChild = map.get(NavbarRegion);

  return (
    <div className={classes.root}>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar className={classes.toolbar}>{navbarChild}</Toolbar>
      </AppBar>
      <main className={classes.content}>{contentChild}</main>
    </div>
  );
}

const styles = (theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    height: '100vh',
  },
  appBar: {
    height: headerHeight,
  },
  toolbar: {
    padding: theme.spacing(0, 2),
    height: '100%',
  },
  content: {
    flex: 1,
    marginTop: headerHeight,
    overflow: 'auto',
    maxHeight: `calc(100vh - ${headerHeight}px)`,
    position: 'relative',
  },
  overlayScrollbar: {
    height: 'calc(100% - 1px)',
  },
});

const useStyles = makeStyles(styles, { name: 'MainLayout' });
