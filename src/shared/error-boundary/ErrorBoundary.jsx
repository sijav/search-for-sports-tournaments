import React from 'react';
import { connect } from 'react-redux';
import { resetErrorAction } from 'src/core/actions';

class ErrorBoundary extends React.Component {
  static getDerivedStateFromError(error) {
    return { error };
  }

  retry = () => {
    const resetError = this.props.errorState.reset;
    this.props.resetError();
    if (typeof resetError === 'function') {
      resetError();
    }
  };

  componentDidCatch(error, errorInfo) {
    console.error(error, errorInfo);
  }

  render() {
    return this.props.errorState.error
      ? this.props.fallback
      : this.props.children;
  }
}

const mapStateToProps = (state) => ({ errorState: state.error });

const mapDispatchToProps = (dispatch) => ({
  resetError: () => dispatch(resetErrorAction()),
});

export const ConnectedErrorBoundary = connect(
  mapStateToProps,
  mapDispatchToProps
)(ErrorBoundary);
