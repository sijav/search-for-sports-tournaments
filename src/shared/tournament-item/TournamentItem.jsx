import {
  CardContent,
  CardMedia,
  IconButton,
  Typography,
  makeStyles,
} from '@material-ui/core';
import React, { useCallback } from 'react';

import DeleteOutlined from '@material-ui/icons/DeleteOutlined';
import clsx from 'clsx';

const BASE_IMAGE_URL = process.env.REACT_APP_BASE_IMAGE_URL;

export function TournamentItem(props) {
  const { title, subtitle, image, onClick, onDelete, id } = props;
  const classes = useStyles(props);

  const handleDelete = useCallback(() => onDelete(id), [onDelete, id]);

  return (
    <div
      className={clsx(classes.root, onClick && classes.rootWithClick)}
      onClick={onClick}
    >
      {image && (
        <CardMedia
          className={classes.cover}
          image={BASE_IMAGE_URL + image}
          title={props.title}
        />
      )}
      <div
        className={clsx(
          classes.details,
          onDelete && classes.detailsWithDeleteIcon
        )}
      >
        <CardContent className={classes.content}>
          <Typography component="h5" variant="h5" className={classes.title}>
            {title}
          </Typography>
          <Typography
            variant="subtitle1"
            color="textSecondary"
            className={classes.subtitle}
          >
            {subtitle}
          </Typography>
        </CardContent>
      </div>
      {onDelete && (
        <IconButton className={classes.deleteIcon} onClick={handleDelete}>
          <DeleteOutlined />
        </IconButton>
      )}
    </div>
  );
}

const styles = (theme) => ({
  root: {
    display: 'flex',
    padding: theme.spacing(0, 1),
    position: 'relative',
    [theme.breakpoints.up('sm')]: {
      position: 'static',
      padding: theme.spacing(0, 2),
    },
    alignItems: 'center',
  },
  rootWithClick: {
    cursor: 'pointer',
  },
  details: {
    flex: '1 1 auto',
    display: 'flex',
    flexDirection: 'column',
    width: 'calc(100vw - 90px)',
    [theme.breakpoints.up('sm')]: {
      width: 400,
    },
  },
  detailsWithDeleteIcon: {
    width: 'calc(100vw - 90px)',
    [theme.breakpoints.up('sm')]: {
      width: 400,
    },
  },
  content: {
    flex: '1 1 auto',
    overflow: 'hidden',
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing(2.5, 2.5, 1, 1),
    },
  },
  title: {
    flex: '1 1 auto',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
  },
  subtitle: {
    flex: '1 1 auto',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
  },
  cover: {
    flex: '0 0 auto',
    width: 75,
    height: 75,
  },
  deleteIcon: {
    flex: '0 0 auto',
    color: theme.palette.error.dark,
    [theme.breakpoints.down('xs')]: {
      position: 'absolute',
      top: 0,
      right: 0,
      padding: theme.spacing(0),
    },
  },
});

const useStyles = makeStyles(styles);
