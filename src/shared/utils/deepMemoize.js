import { useEffect, useRef } from 'react';

import { isJsonEqual } from './isJsonEqual';

const getValue = (oldValue, newValue) =>
  isJsonEqual(oldValue, newValue) ? oldValue : newValue;

export function useDeepMemoize(value) {
  const ref = useRef(value);
  useEffect(() => {
    ref.current = getValue(ref.current, value);
  });
  return getValue(ref.current, value);
}
