import axios from 'axios';

const BASE_URL = process.env.REACT_APP_BASE_URL;

export function getClient(dispatchSuccess, dispatchError) {
  const cancelTokenSource = axios.CancelToken.source();
  const cancel = cancelTokenSource.cancel;
  const cancelToken = cancelTokenSource.token;
  const catchError = (axiosError) =>
    !axios.isCancel(axiosError) && dispatchError(axiosError);
  const get = (url, config) =>
    axios
      .get(BASE_URL + url, { ...config, cancelToken })
      .then((res) => res.data)
      .then(dispatchSuccess)
      .catch(catchError);
  return { cancel, get };
}

export function getUniqueClient() {
  let client;
  let cancel = () => {};
  const get = (dispatchSuccess, dispatchError) => (url, config) => {
    if (cancel) {
      cancel('Operation canceled due to new request.');
    }
    client = getClient(dispatchSuccess, dispatchError);
    cancel = client.cancel;
    client.get(url, config);
  };
  return { cancel, get };
}
