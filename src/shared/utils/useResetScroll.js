import { useEffect, useRef } from 'react';

import { useLocation } from 'react-router-dom';

export const useResetScroll = () => {
  const location = useLocation();
  const ref = useRef(null);

  useEffect(() => {
    const overlayScrollbar = ref.current?.getOverlayScrollbar();
    // eslint-disable-next-line no-unused-expressions
    overlayScrollbar?.scroll(0);
  }, [location]);

  return ref;
};
