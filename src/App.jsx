import { AppRouter } from './AppRouter';
import { CssBaseline } from '@material-ui/core';
import { GlobalStyles } from 'src/core/styles';
import { MainThemeProvider } from 'src/core/themes';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import React from 'react';
import { configureMainStore } from 'src/core/stores';

const { store, persistor } = configureMainStore();

export function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <MainThemeProvider>
          <GlobalStyles />
          <CssBaseline />
          <AppRouter />
        </MainThemeProvider>
      </PersistGate>
    </Provider>
  );
}
